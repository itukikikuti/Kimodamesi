﻿using UnityEngine;

/// <summary>
/// プレイヤーの移動処理
/// </summary>
public class PlayerMove : MonoBehaviour
{
	public float scale = 0.1f;	// 移動速度

	/// <summary>
	/// 初期化処理
	/// </summary>
	void Awake()
	{

	}

	/// <summary>
	/// 更新処理
	/// </summary>
	void Update()
	{
		// Wキーが押されていた場合
		if (Input.GetKey(KeyCode.W))
		{
			// 前に進む
			transform.position += transform.forward * scale;
		}

		// Sキーが押されていた場合
		if (Input.GetKey(KeyCode.S))
		{
			// 後ろに進む
			transform.position -= transform.forward * scale;
		}


		// Aキーが押されていた場合
		if (Input.GetKey(KeyCode.A))
		{
			// 左に進む
			transform.position -= transform.right * scale;
		}

		// Dキーが押されていた場合
		if (Input.GetKey(KeyCode.D))
		{
			// 右に進む
			transform.position += transform.right * scale;
		}
	}
}
