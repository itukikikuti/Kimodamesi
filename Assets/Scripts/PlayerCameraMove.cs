﻿using UnityEngine;

/// <summary>
/// プレイヤーのカメラの移動処理
/// </summary>
public class PlayerCameraMove : MonoBehaviour
{
	public float scale = 1f;					// 視点移動の速さ
	public float minVerticalAngle = -90f;		// 縦の最小角度
	public float maxVerticalAngle = 90f;		// 縦の最大角度
	private float verticalAngle = 0f;			// 縦の角度
	private float horizontalAngle = 0f;			// 横の角度

	public Transform playerTransform = null;	// プレイヤーのトランスフォーム

	/// <summary>
	/// 初期化処理
	/// </summary>
	void Awake()
	{
		// カーソルを固定する
		Cursor.lockState = CursorLockMode.Locked;
		
		// マウスを非表示にする
		Cursor.visible = false;
    }

	/// <summary>
	/// 更新処理
	/// </summary>
	void Update()
	{
		// 縦の角度を更新する
		verticalAngle -= Input.GetAxis("Mouse Y") * scale;

		// 横の角度を更新する
		horizontalAngle += Input.GetAxis("Mouse X") * scale;

		// 視点の縦角度を制限する
		verticalAngle = Mathf.Clamp(verticalAngle, minVerticalAngle, maxVerticalAngle);

		// 視点移動する
		transform.eulerAngles = new Vector3(verticalAngle, transform.eulerAngles.y, 0f);

		// プレイヤーの向きを変更する
		playerTransform.eulerAngles = new Vector3(0f, horizontalAngle, 0f);
	}
}
