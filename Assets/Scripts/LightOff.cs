﻿using UnityEngine;

/// <summary>
/// ライトを消す処理
/// </summary>
public class LightOff : MonoBehaviour
{
	/// <summary>
	/// 初期化処理
	/// </summary>
	void Awake()
	{
		// ライトを消す
		GetComponent<Light>().intensity = 0f;
	}
}
